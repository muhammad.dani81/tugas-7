from django.test import TestCase

import unittest
import time

from django.test import TestCase, Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .views import *

class UnitTest(TestCase):

	def test_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	def test_url_that_does_not_exist(self):
		response = Client().get('/nothing/')
		self.assertEqual(response.status_code, 404)

	def test_used_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'tugas7.html')

	def test_text_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', content)
		self.assertIn('Mau ganti tema?', content)
	def test_list_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<ul', content)
	def test_button_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<button', content)
		self.assertIn('Ganti', content)

	def test_page_uses_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, javascript) #Views's function

# class SeleniumFunctionalTest(TestCase):

# 	def setUp(self):
# 		options = Options()
# 		options.add_argument('--headless')
# 		self.selenium = webdriver.Firefox(firefox_options=options)
# 		super(SeleniumFunctionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(SeleniumFunctionalTest,self).tearDown()