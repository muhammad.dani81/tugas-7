function gantiTema(){
    var body = $('body');
    var header = $('.top');
    var judul = $('h2');
    var button = $('button')

    if (button.attr('id') == "buttOn"){
        body.css("background-color", "#e5e5ff");
        header.css("background-color", "#4c4cff");
        judul.css("color", "black")

        button.attr('id', "buttOff");
    }
    else {
        body.css("background-color", "white");
        header.css("background-color", "black");
        judul.css("color", "white")

        button.attr('id', "buttOn");
    }
    return false;
}

